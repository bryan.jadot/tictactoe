export const Character = {
    X: "X",
    O: "O",
}

export const PlayerType = {
    Local: "Local",
    AI: "AI",
    Remote: "Remote"
}

class Player {
    constructor(character) {
        this._character = character;
    }

    get character() {
        return this._character
    }
}

export class LocalPlayer extends Player {
    get type() {
        return PlayerType.Local;
    }
}