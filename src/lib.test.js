import { Character, LocalPlayer } from './Player'
import { getWinState, WinState } from './lib'

const generateOwnersForInd = (inds, forPlayer) => {
    const cellOwners = [...Array(9).keys()].map(() => null);
    inds.forEach(i => {cellOwners[i] = forPlayer});

    return cellOwners;
}

const testForWinningInds = (inds) => {
    inds = inds.sort();
    const xPlayer = new LocalPlayer(Character.X);
    const oPlayer = new LocalPlayer(Character.O);

    let owners = generateOwnersForInd(inds, xPlayer);
    let winState = getWinState(owners);
    expect(winState.winState).toBe(WinState.X);
    expect(winState.winningInds.sort()).toEqual(inds);

    owners = generateOwnersForInd(inds, oPlayer);
    winState = getWinState(owners);
    expect(winState.winState).toBe(WinState.O);
    expect(winState.winningInds.sort()).toEqual(inds);
}

describe('lib', () => {
    describe('getWinState', () => {
        it ('should work for rows', () => {
            for (let i = 0; i < 3; i++) {
                testForWinningInds([i + 0, i + 3, i + 6]);
            }
        })

        it ('should work for cols', () => {
            for (let i = 0; i < 3; i++) {
                testForWinningInds([3 * i + 0, 3 * i + 1, 3 * i + 2]);
            }
        })

        it ('should work for diagonals', () => {
            testForWinningInds([0, 4, 8]);
            testForWinningInds([2, 4, 6]);
        })

        it ('should work for hung', () => {
            const xPlayer = new LocalPlayer(Character.X);
            const oPlayer = new LocalPlayer(Character.O);

            let testOwners = [
                xPlayer, oPlayer, xPlayer,
                xPlayer, oPlayer, xPlayer,
                oPlayer, xPlayer, oPlayer
            ];
            let winState = getWinState(testOwners);
            expect(winState.winState).toBe(WinState.HUNG);
            expect(winState.winningInds).toBeNull();
        })

        it ('should work for no wins', () => {
            const xPlayer = new LocalPlayer(Character.X);
            const oPlayer = new LocalPlayer(Character.O);

            let testOwners = [
                xPlayer, oPlayer, xPlayer,
                xPlayer, oPlayer, xPlayer,
                oPlayer, null, oPlayer
            ];
            let winState = getWinState(testOwners);
            expect(winState.winState).toBe(WinState.NO_WIN);
            expect(winState.winningInds).toBeNull();
        })
    })
})