import Board from './Board'
import React from 'react';
import PropTypes from 'prop-types';
import styles from './Game.module.css'
import TopBar from './TopBar'


const Game = ({winState, currentPlayer, cellStates, highlightCells, disableMarking, markCell, startNewGame}) => {
    return (
        <div className={styles.game}>
            <TopBar
                winState={winState}
                currentPlayer={currentPlayer}
                startNewGame={startNewGame}
            />
            <Board
                cellStates={cellStates}
                highlightCells={highlightCells}
                disableMarking={disableMarking}
                markCell={markCell}
            />
        </div>
    );
}

Game.propTypes = {
    winState: PropTypes.string.isRequired,
    currentPlayer: PropTypes.string.isRequired,
    cellStates: PropTypes.arrayOf(
        PropTypes.shape({
            index: PropTypes.number.isRequired,
            text: PropTypes.string.isRequired
        })
    ),
    highlightCells: PropTypes.arrayOf(PropTypes.number),
    disableMarking: PropTypes.bool.isRequired,
    markCell: PropTypes.func.isRequired,
    startNewGame: PropTypes.func.isRequired,
}

export default Game;
