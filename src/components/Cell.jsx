import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import styles from './Cell.module.css'

const Cell = ({index, text, highlight, disabled, onClick}) => {
    const classes = classNames(styles.cell, {[styles.highlight]: highlight});
    return <button disabled={disabled} onClick={onClick} className={classes}>{text}</button>;
}

Cell.propTypes = {
    index: PropTypes.number.isRequired,
    text: PropTypes.string.isRequired,
    highlight: PropTypes.bool.isRequired,
    disabled: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired
}

export default Cell;
