import classNames from 'classnames';
import React from 'react';
import PropTypes from 'prop-types';
import styles from './TopBar.module.css';
import { WinState } from '../lib';

const TopBar = ({winState, currentPlayer, startNewGame}) => {
    let gameInfo = "";
    if (winState === WinState.NO_WIN) {
        gameInfo = <span>{currentPlayer}'s turn</span>;
    } else if (winState === WinState.HUNG) {
        gameInfo = <span>Hung game!</span>;
    } else if (winState === WinState.X) {
        gameInfo = <span>X wins!</span>;
    } else if (winState === WinState.O) {
        gameInfo = <span>O wins!</span>;
    } else {
        throw new Error("Invalid WinState!");
    }

    return (
        <div className={styles.topBar}>
            <span className={styles.topBarFont}>{gameInfo}</span>
            <button className={classNames(styles.topBarFont, styles.button)} onClick={startNewGame}>New game</button>
        </div>
    );
}

TopBar.propTypes = {
    winState: PropTypes.string.isRequired,
    currentPlayer: PropTypes.string.isRequired,
    startNewGame: PropTypes.func.isRequired
}

export default TopBar;
