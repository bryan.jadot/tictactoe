import Cell from './Cell'
import React from 'react';
import PropTypes from 'prop-types';

import styles from './Board.module.css'

const Board = ({cellStates, highlightCells, disableMarking, markCell}) => {
    const squares = cellStates.map(
        (cellState) => (
            <Cell
                index={cellState.index}
                text={cellState.text}
                onClick={() => markCell(cellState.index)}
                key={cellState.index}
                highlight={highlightCells.includes(cellState.index)}
                disabled={disableMarking}
            />
        )
    );
    return <div className={styles.board}>{squares}</div>;
}

Board.propTypes = {
    cellStates: PropTypes.arrayOf(
        PropTypes.shape({
            index: PropTypes.number.isRequired,
            text: PropTypes.string.isRequired
        })
    ),
    highlightCells: PropTypes.arrayOf(PropTypes.number),
    disableMarking: PropTypes.bool.isRequired,
    markCell: PropTypes.func.isRequired
}

export default Board;
