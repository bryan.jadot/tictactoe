import { Character } from '../Player';
import { markCell, startNewGame } from '../actions'
import { connect } from 'react-redux'
import { getWinState, WinState } from '../lib'
import Game from "../components/Game"

const mapStateToProps = state => {
    let winState = getWinState(state.cellOwners);
    return {
        winState: winState.winState,
        highlightCells: (winState.winningInds === null) ? [] : winState.winningInds,
        currentPlayer: state.currentPlayer.character,
        disableMarking: winState.winState !== WinState.NO_WIN,
        cellStates: state.cellOwners.map(
            (owner, i) => ({
                index: i,
                text: (owner === null) ? " " : ((owner.character === Character.X) ? "X" : "O")
            })
        )
    }
}

const mapDispatchToProps = dispatch => ({
    markCell: index => dispatch(markCell(index)),
    startNewGame: () => dispatch(startNewGame())
})

export default connect(mapStateToProps, mapDispatchToProps)(Game);