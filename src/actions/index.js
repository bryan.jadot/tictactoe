export const markCell = (index) => ({
    type: 'MARK_CELL',
    index
})

export const startNewGame = (players) => ({
    type: "START_NEW_GAME",
    players
})

