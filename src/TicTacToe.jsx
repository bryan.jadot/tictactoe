import React, { Component } from 'react'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import { board } from './reducers/board'
import GameContainer from './containers/GameContainer'

class TicTacToe extends Component {
    constructor(props) {
        super(props);
        this.store = createStore(board);
    }

    render() {
        return (
            <Provider store={this.store}><GameContainer /></Provider>
        );
    }
}

export default TicTacToe;