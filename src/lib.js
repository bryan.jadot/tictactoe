import { Character } from "./Player";

export const WinState = {
    NO_WIN: "NO_WIN",
    HUNG: "HUNG",
    ...Character
}

export const getWinState = (cellOwners) => {
    const xyToInd = (x, y) => y * 3 + x;
    const oneToThree = [...Array(3).keys()]
    const indsAreWinning = (inds) => {
        if (cellOwners[inds[0]] === null) {
            return false;
        }
        return inds.every(i => cellOwners[i] === cellOwners[inds[0]]);
    }
    let candidateInds = null;

    for (let x = 0; x < 3; x++) {
        candidateInds = oneToThree.map(y => xyToInd(x, y));
        if (indsAreWinning(candidateInds)) {
            return {
                winState: cellOwners[candidateInds[0]].character,
                winningInds: candidateInds
            }
        }
    }

    for (let y = 0; y < 3; y++) {
        candidateInds = oneToThree.map(x => xyToInd(x, y));
        if (indsAreWinning(candidateInds)) {
            return {
                winState: cellOwners[candidateInds[0]].character,
                winningInds: candidateInds
            }
        }
    }

    candidateInds = oneToThree.map(x => xyToInd(x, x));
    if (indsAreWinning(candidateInds)) {
        return {
            winState: cellOwners[candidateInds[0]].character,
            winningInds: candidateInds
        }
    }

    candidateInds = oneToThree.map(x => xyToInd(x, 2 - x));
    if (indsAreWinning(candidateInds)) {
        return {
            winState: cellOwners[candidateInds[0]].character,
            winningInds: candidateInds
        }
    }

    if (cellOwners.every(owner => owner !== null)) {
        return {
            winState: WinState.HUNG,
            winningInds: null
        }
    } else {
        return {
            winState: WinState.NO_WIN,
            winningInds: null
        }
    }
}