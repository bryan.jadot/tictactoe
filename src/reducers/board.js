import { LocalPlayer, Character } from '../Player';

const defaultPlayers = () => {
    return {
        [Character.X]: new LocalPlayer(Character.X),
        [Character.O]: new LocalPlayer(Character.O)
    };
}

const generateStartingBoard = (players=defaultPlayers()) => {
    return {
        currentPlayer: players[Character.X],
        players: players,
        cellOwners: [...Array(9).keys()].map((index) => null)
    };
}

export const board = (state=generateStartingBoard(), action) => {
    switch (action.type) {
        case 'START_NEW_GAME':
            return generateStartingBoard(action.players);
        case 'MARK_CELL':
            const newCellOwners = [...state.cellOwners];
            let newCurrentPlayer = state.currentPlayer;

            if (state.cellOwners[action.index] === null) {
                newCellOwners[action.index] = state.currentPlayer;

                if (state.currentPlayer.character === Character.X) {
                    newCurrentPlayer = state.players[Character.O];
                } else {
                    newCurrentPlayer = state.players[Character.X];
                }
            }
            return {
                ...state,
                currentPlayer: newCurrentPlayer,
                cellOwners: newCellOwners
            };
        default:
            return state
    }
}
